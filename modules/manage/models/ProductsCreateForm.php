<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\manage\models;

use Yii;
use yii\base\Model;
use yii\base\Response;
use app\modules\manage\behaviors\SessionStaticBehavior;

/**
 * Description of ProductsCreateForm
 *
 * @author mezinn
 */
class ProductsCreateForm extends Model {

    public $name;

    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

}
