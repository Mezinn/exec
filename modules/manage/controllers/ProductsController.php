<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\manage\controllers;

use Yii;
use yii\web\Controller;
use app\modules\manage\models\search\ProductSearch;
use app\modules\manage\models\ProductsCreateForm;

/**
 * Description of ProductsController
 *
 * @author mezinn
 */
class ProductsController extends Controller {

    public function actionIndex() {
        $searchModel = new ProductSearch;
        $model = new ProductsCreateForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    public function actionCreate() {
        return $this->render('create');
    }

}
