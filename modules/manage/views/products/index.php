<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .action-list  li:hover{
        background-color: lightgrey !important; 
    }
</style>
<div class="products-index row">
    <div class="col-md-3">
        <ul class="list-group action-list">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?= Html::a(Yii::t('app', 'Create new'), '#', ['onclick' => '$("#create-new").modal("show")']) ?>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <?= Html::a(Yii::t('app', ' Drafts'), '#') ?>
                <span class="badge badge-primary badge-pill">1</span>
            </li>
        </ul>
    </div>
    <div class="col-md-9">
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'products']); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'name',
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
<?php
Modal::begin([
    'id' => 'create-new',
    'header' => '<h3>Enter name of new product</h3>',
    'toggleButton' => false,
])
?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'create-new-product']) ?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]) ?>
<?= $form->field($model, 'name') ?>
<div class="row">
    <div class="col-md-2 col-sm-2 col-xs-3">
        <?= Html::submitButton(Yii::t('app', 'Next step'), ['class' => 'btn btn-success inline']) ?>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-3">
        <?= Html::tag('p', 'Cancel', ['class' => 'btn btn-primary', 'onclick' => '$("#create-new").modal("hide")']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
<?php Pjax::end() ?>
<?php Modal::end() ?>
