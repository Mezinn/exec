<?php

use yii\db\Migration;

/**
 * Class m180727_140428_init
 */
class m180727_140428_init extends Migration {

    public function up() {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('colors', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%colors}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'name' => 'VARCHAR(255) NOT NULL',
                        ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('products', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%products}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'name' => 'VARCHAR(255) NOT NULL',
                    'desc' => 'TEXT NOT NULL',
                    'slug' => 'VARCHAR(255) NOT NULL',
                    'show_on_website' => 'TINYINT(1) NOT NULL',
                        ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('products_instances', $tables)) {
            if ($dbType == "mysql") {
                $this->createTable('{{%products_instances}}', [
                    'product_id' => 'INT(11) NOT NULL',
                    'color_id' => 'INT(11) NOT NULL',
                    'price' => 'DECIMAL(10,2) NOT NULL',
                    'amount' => 'INT(11) NOT NULL',
                    'show_on_website' => 'TINYINT(1) NOT NULL',
                        ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_product_id_8646_00', 'products_instances', 'product_id', 0);
        $this->createIndex('idx_color_id_8646_01', 'products_instances', 'color_id', 0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_products_instances_8586_00', '{{%colors}}', 'id', '{{%products_instances}}', 'color_id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_products_8636_01', '{{%products_instances}}', 'product_id', '{{%products}}', 'id', 'CASCADE', 'NO ACTION');
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down() {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `colors`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `products`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `products_instances`');
        $this->execute('SET foreign_key_checks = 1;');
    }

}
