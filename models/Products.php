<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property string $slug
 * @property int $show_on_website
 *
 * @property ProductsInstances[] $productsInstances
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'slug'], 'required'],
            [['desc'], 'string'],
            [['show_on_website'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'desc' => Yii::t('app', 'Desc'),
            'slug' => Yii::t('app', 'Slug'),
            'show_on_website' => Yii::t('app', 'Show On Website'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsInstances()
    {
        return $this->hasMany(ProductsInstances::className(), ['product_id' => 'id']);
    }
}
